// ==UserScript==
// @name         Only 人狼
// @namespace    https://132a0.com/
// @version      2024-11-26
// @description  try to take over the world!
// @author       beflat
// @match        https://zinro.net/m/room_list.php*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=zinro.net
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const table = document.querySelector(".table-bordered").children[0].children;
    [...table].map((el) => {
        const includetag = [...el.children[0].children].filter((el2) => {return el2.innerText === '人狼系'});
        const excludetag = [...el.children[0].children].filter((el2) => {return el2.innerText === '雑談系' || el2.innerText === '身内' || el2.innerText === 'ワードウルフ' || el2.innerText === 'ワンナイト'});
        const islock = el.children[0].children[0].getAttribute("class") === null
        if(islock || includetag.length == 0 || excludetag.length > 0){
            el.remove()
        }
    })

})();