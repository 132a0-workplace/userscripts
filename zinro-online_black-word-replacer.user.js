// ==UserScript==
// @name         Black Word Replacer
// @namespace    https://132a0.com/
// @version      2024-11-27
// @description  try to take over the world!
// @author       132a0
// @match        https://zinro.net/m/player.php*
// @match        https://zinro.net/m/log.php*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=zinro.net
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const regex = /\u0300|\u0301|\u0302|\u0307|\u0309|\u030c|\u030d|\u030b|\u030e|\u0310|\u0311|\u0313|\u0316|\u031c|\u031f|\u0320|\u0323|\u0324|\u0326|\u032b|\u032c|\u032f|\u0330|\u0331|\u0332|\u0333|\u0334|\u0336|\u033d|\u033e|\u033f|\u0345|\u0346|\u034b|\u034e|\u0351|\u0352|\u0353|\u0359|\u035b|\u0488|\u0489|\u0e37|\u0e47|\u0e49|\u0e4e/gi

    const main = document.querySelector("#message");
    const messages = main.children[0].children
    const messages2 = main.children
    Object.values(messages).map((elem) => {
        if(!!(elem.children[1])){
            const text = elem.children[1]
            text.innerText = text.innerText.replaceAll(regex, "")
        }
    })
    Object.values(messages2).map((elem) => {
        if(!!(elem.children[1])){
            const text = elem.children[1]
            if(elem.children[0].innerText !== "鯖"){
                text.innerText = text.innerText.replaceAll(regex, "")
            }

        }
    })
    const config = { attributes: true, childList: true, subtree: true };
    const callback = (mutationList, observer) => {

        mutationList.map(mutate => {

            if(!!(mutate.addedNodes[0].children)){
                const text = mutate.addedNodes[0].children[1];
                text.innerText = text.innerText.replaceAll(regex, "")
            }
        })
    }
    const observer = new MutationObserver(callback, { attributes: true, childList: true, subtree: true });
    observer.observe(main, config);
    // Your code here...
})();