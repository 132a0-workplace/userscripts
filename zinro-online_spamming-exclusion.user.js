// ==UserScript==
// @name         Spamming Exclusion
// @namespace    https://132a0.com/
// @version      2024-11-26
// @description  try to take over the world!
// @author       132a0
// @match        https://zinro.net/m/player.php*
// @match        https://zinro.net/m/log.php*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=zinro.net
// @grant        none
// ==/UserScript==

const getSpams = () => {
    const spams = window.localStorage.getItem("spam-names");
        if(spams){
        return JSON.parse(spams);
    }
    return []
}


const switchSpam = ( name ) => {
    const spams = getSpams()
    if(spams.includes(name)){
        const index = spams.indexOf(name);
        spams.splice(index, 1);
    }else{
        spams.push(name)
    }
    window.localStorage.setItem("spam-names", JSON.stringify(spams));
}

const resetSpam = () => {
    window.localStorage.setItem("spam-names", JSON.stringify([]));
}

const create_spamlist = () => {
    const spams = getSpams
}

const create_checkbox = (name, id) => {
    const sub = document.createElement('div')

    const subinput = document.createElement('label')
    const subinputCheckbox = document.createElement('input')
    subinputCheckbox.setAttribute('type', 'radio')
    subinputCheckbox.setAttribute('name', 'trip')
    subinputCheckbox.setAttribute('id', id)
    subinputCheckbox.addEventListener('change', (el) => {
        const name = document.querySelector('input#name-data').value;
        el.preventDefault();
        if(subinputCheckbox.checked){
            if(id === 'switch-spam'){
                switchSpam(name);
            }else if(id === 'reset'){
                resetSpam();
            }

        }else{
            if(id === 'switch-spam'){
                switchSpam(name);
            }
        }
    })
    const subinputName = document.createTextNode(name)
    subinput.appendChild(subinputCheckbox)
    subinput.appendChild(subinputName)
    sub.appendChild(subinput)


    return sub;
}

const create_form = () => {
    const main = document.createElement('form')
    main.classList.add("spam-modal")
    const title = document.createElement('h4')
    title.classList.add("title")
    title.innerText = "Spamming Exclusion"
    //<h4 class="title">設定</h4>
    const namearea = document.createElement('input')
    namearea.setAttribute('type', 'text')
    namearea.setAttribute('id', 'name-data')
    namearea.setAttribute('placeholder', 'name')

    const submission = create_checkbox("Add or Remove as Spam", "switch-spam")
    const reset = create_checkbox("Reset", "reset-spam")

    main.appendChild(title)
    main.appendChild(namearea);
    main.appendChild(submission);
    main.appendChild(reset);
    return main;
}

const spamCheck = (nameTag) => {
    if(!nameTag) return false
    const spams = getSpams()
    const name = nameTag.innerText.split("→")[0]
    return spams.includes(name)
}

(function() {
    'use strict';

    const main = document.querySelector("#message");
    const messages = main.children[0].children
    const messages2 = main.children

    Object.values(messages).map((elem) => {
        const user = elem.children[0]
        const message = elem.children[1]
        if(spamCheck(user)){
            message.innerText = `: (スパムの可能性があります(文字数 ${message.innerText.length}))`
        }
    })
    Object.values(messages2).map((elem) => {
        const user = elem.children[0]
        const message = elem.children[1]
        if(spamCheck(user)){
            message.innerText = `: (スパムの可能性があります(文字数 ${message.innerText.length}))`
        }
    })

    const callback = (mutationList, observer) => {
        const spammodal = document.querySelector(".spam-modal")
        if(!spammodal){
            const modal = create_form();
            const area = document.querySelector(".row-fluid").children[1];
            area.appendChild(modal);
        }

        mutationList.map(mutate => {

            if(!!(mutate.addedNodes[0].children)){
                const user = mutate.addedNodes[0].children[0];
                const message = mutate.addedNodes[0].children[1];
                if(spamCheck(user)){
                    message.innerText = `: (スパムの可能性があります(文字数 ${message.innerText.length}))`
                }
            }
        })
    }

    const spammodal = document.querySelector(".spam-modal")
    if(!spammodal){
        const modal = create_form();
        const area = document.querySelector(".row-fluid").children[1];
        area.appendChild(modal);
    }

    const config = { attributes: true, childList: true, subtree: true };
    const observer = new MutationObserver(callback, { attributes: true, childList: true, subtree: true });
    observer.observe(main, config);
    // Your code here...
})();