// ==UserScript==
// @name         Trip Traverser in Zinro Online
// @namespace    https://132a0.com/
// @version      2024-11-26
// @description  try to take over the world!
// @author       132a0
// @match        https://zinro.net/m/player.php
// @icon         https://www.google.com/s2/favicons?sz=64&domain=zinro.net
// @grant        none
// ==/UserScript==

//TODO: 酉を動的に追加する機能
const tripregex = new RegExp("((◆)?[0-9a-zA-Z\/\.]{10,12})\<br");

// アイコン一覧
// https://fontawesome.com/v3/icon

class trip{
    //TODO
}

const removeTrip = (id) => {
    removeBrokenTrip(id);
    removeUnrestTrip(id);
    removeSpamTrip(id);
    removeVerifiedTrip(id);

}

const getBrokenTrip = () => {
    const trip = window.localStorage.getItem("trip-traverser-broken-trip");
    if(trip){
        return JSON.parse(trip);
    }
    return []
}

const addBrokenTrip = (id) => {
    removeTrip(id);
    const trip = getBrokenTrip();
    if(!trip.includes(id)){
        trip.push(id);
    }
    window.localStorage.setItem("trip-traverser-broken-trip", JSON.stringify(trip));
}

const removeBrokenTrip = (id) => {
    const trip = getBrokenTrip();
    if(trip.includes(id)){
        const index = trip.indexOf(id);
        trip.splice(index, 1);
    }
    window.localStorage.setItem("trip-traverser-broken-trip", JSON.stringify(trip));
}

const getUnrestTrip = () => {
    const trip = window.localStorage.getItem("trip-traverser-unrest-trip");
    if(trip){
        return JSON.parse(trip);
    }
    return []
}

const addUnrestTrip = (id) => {
    removeTrip(id);
    const trip = getUnrestTrip();
    if(!trip.includes(id)){
        trip.push(id);
    }
    window.localStorage.setItem("trip-traverser-unrest-trip", JSON.stringify(trip));
}

const removeUnrestTrip = (id) => {
    const trip = getUnrestTrip();
    if(trip.includes(id)){
        const index = trip.indexOf(id);
        trip.splice(index, 1);
    }
    window.localStorage.setItem("trip-traverser-unrest-trip", JSON.stringify(trip));
}

const getSpamTrip = () => {
    const trip = window.localStorage.getItem("trip-traverser-spam-trip");
    if(trip){
        return JSON.parse(trip);
    }
    return []
}

const addSpamTrip = (id) => {
    removeTrip(id);
    const trip = getSpamTrip();
    if(!trip.includes(id)){
        trip.push(id);
    }
    window.localStorage.setItem("trip-traverser-spam-trip", JSON.stringify(trip));
}

const removeSpamTrip = (id) => {
    const trip = getSpamTrip();
    if(trip.includes(id)){
        const index = trip.indexOf(id);
        trip.splice(index, 1);
    }
    window.localStorage.setItem("trip-traverser-spam-trip", JSON.stringify(trip));
}

const getVerifiedTrip = () => {
    const trip = window.localStorage.getItem("trip-traverser-verified-trip");
    if(trip){
        return JSON.parse(trip);
    }
    return []
}

const addVerifiedTrip = (id) => {
    removeTrip(id);
    const trip = getVerifiedTrip();
    if(!trip.includes(id)){
        trip.push(id);
    }
    window.localStorage.setItem("trip-traverser-verified-trip", JSON.stringify(trip));
}

const removeVerifiedTrip = (id) => {
    const trip = getVerifiedTrip();
    if(trip.includes(id)){
        const index = trip.indexOf(id);
        trip.splice(index, 1);
    }
    window.localStorage.setItem("trip-traverser-verified-trip", JSON.stringify(trip));
}

const icon_verified = () => {
    const el = document.createElement('i');
    el.classList.add('icon-ok')
    return el;
}

const icon_broken = () => {
    const el = document.createElement('i');
    el.classList.add('icon-fire')
    return el;
}

const icon_unrest = () => {
    const el = document.createElement('i');
    el.classList.add('icon-warning-sign')
    return el;
}

const icon_spam = () => {
    const el = document.createElement('i');
    el.classList.add('icon-thumbs-down')
    return el;
}

const create_checkbox = (name, id) => {
    const sub = document.createElement('div')

    const subinput = document.createElement('label')
    const subinputCheckbox = document.createElement('input')
    subinputCheckbox.setAttribute('type', 'radio')
    subinputCheckbox.setAttribute('name', 'trip')
    subinputCheckbox.setAttribute('id', id)
    subinputCheckbox.addEventListener('change', (el) => {
        const trip = document.querySelector('input#trip-data').value;
        el.preventDefault();
        if(subinputCheckbox.checked){
            if(id === 'broken'){
                addBrokenTrip(trip);
            }else if(id === 'unrest'){
                addUnrestTrip(trip);
            }else if(id === 'spam'){
                addSpamTrip(trip);
            }else if(id === 'verified'){
                addVerifiedTrip(trip);
            }

        }else{
            if(id === 'broken'){
                removeBrokenTrip(trip);
            }else if(id === 'unrest'){
                removeUnrestTrip(trip);
            }else if(id === 'spam'){
                removeSpamTrip(trip);
            }else if(id === 'verified'){
                removeVerifiedTrip(trip);
            }
        }
    })
    const subinputName = document.createTextNode(name)
    subinput.appendChild(subinputCheckbox)
    subinput.appendChild(subinputName)
    sub.appendChild(subinput)


    return sub;
}

const list_append_area = () => {
    const main = document.createElement('form')
    main.classList.add("trip-modal")

    const triparea = document.createElement('input')
    const sub1 = create_checkbox('割れ酉', 'broken');
    const sub2 = create_checkbox('不穏', 'unrest');
    const sub3 = create_checkbox('スパム', 'spam');
    const sub4 = create_checkbox('安全酉', 'verified');
    triparea.setAttribute('type', 'text')
    triparea.setAttribute('id', 'trip-data')
    triparea.setAttribute('placeholder', 'trip')
    triparea.addEventListener("input", (el) => {
        el.preventDefault();
        const trip_broken = getBrokenTrip();
        const trip_unrest = getUnrestTrip();
        const trip_spam = getSpamTrip();
        const trip_verified = getVerifiedTrip();
        if(trip_broken.includes(triparea.value)){
            sub1.children[0].children[0].checked = true;
            sub2.children[0].children[0].checked = false;
            sub3.children[0].children[0].checked = false;
            sub4.children[0].children[0].checked = false;
        }else if(trip_unrest.includes(triparea.value)){
            sub1.children[0].children[0].checked = false;
            sub2.children[0].children[0].checked = true;
            sub3.children[0].children[0].checked = false;
            sub4.children[0].children[0].checked = false;
        }else if(trip_spam.includes(triparea.value)){
            sub1.children[0].children[0].checked = false;
            sub2.children[0].children[0].checked = false;
            sub3.children[0].children[0].checked = true;
            sub4.children[0].children[0].checked = false;
        }else if(trip_verified.includes(triparea.value)){
            sub1.children[0].children[0].checked = false;
            sub2.children[0].children[0].checked = false;
            sub3.children[0].children[0].checked = false;
            sub4.children[0].children[0].checked = true;
        }else{
            sub1.children[0].children[0].checked = false;
            sub2.children[0].children[0].checked = false;
            sub3.children[0].children[0].checked = false;
            sub4.children[0].children[0].checked = false;
        }

    })

    main.appendChild(triparea);
    main.appendChild(sub1);
    main.appendChild(sub2);
    main.appendChild(sub3);
    main.appendChild(sub4);
    return main;
}

(function() {
    'use strict';

    const main = document.querySelector("#all_players");
    const config = { attributes: true, childList: true, subtree: true };

    const callback = (mutationList, observer) => {
        const tripmodal = document.querySelector(".trip-modal")
        if(!tripmodal){
            const modal = list_append_area();
            const area = document.querySelector(".row-fluid").children[1];
            area.appendChild(modal);
        }


        const users = main.children[0].children[0].children;
        [...users].map((el) => {
            const trip_broken = getBrokenTrip();
            const trip_unrest = getUnrestTrip();
            const trip_spam = getSpamTrip();
            const trip_verified = getVerifiedTrip();


            let trip = el.innerHTML.match(tripregex);
            if(trip)trip = trip[1];
            const icons = el.children[1].children[0];

            //割れ酉
            if(trip_broken.includes(trip)){
                if(icons.children.length == 1){
                    icons.appendChild(icon_broken());
                }
            }

            //不穏酉
            if(trip_unrest.includes(trip)){
                if(icons.children.length == 1){
                    icons.appendChild(icon_unrest());
                }
            }

            //スパム酉
            if(trip_spam.includes(trip)){
                if(icons.children.length == 1){
                    icons.appendChild(icon_spam());
                }
            }

            //信頼出来るトリップ
            if(trip_verified.includes(trip)){
                if(icons.children.length == 1){
                    icons.appendChild(icon_verified());
                }
            }

            return el;
        })


    }

    const observer = new MutationObserver(callback, { attributes: true, childList: true, subtree: true });
    observer.observe(main, config);
})();